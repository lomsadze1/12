package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.app.Person.fromBundle
import androidx.fragment.app.Fragment
import com.example.fragments.R

class Fragment3 : Fragment(R.layout.fragment_3) {

    private lateinit var textView: TextView
    private lateinit var textView1: TextView
    private lateinit var textView2: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textView = view.findViewById(R.id.Passed1)
        textView.text = Fragment3Args.fromBundle(requireArguments()).text

        textView1 = view.findViewById(R.id.Passed2)
        textView1.text = Fragment3Args.fromBundle(requireArguments()).text1

        textView2 = view.findViewById(R.id.Passed3)
        textView2.text = Fragment3Args.fromBundle(requireArguments()).text2

    }

}