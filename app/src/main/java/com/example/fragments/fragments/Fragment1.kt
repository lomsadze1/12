package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.fragments.R

class Fragment1 : Fragment(R.layout.fragment_1) {

    private lateinit var editText: EditText
    lateinit var button1: Button
    lateinit var editText1: EditText
    lateinit var editText2: EditText

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editText = view.findViewById(R.id.enterText)
        button1 = view.findViewById(R.id.enterButton1)

        editText1 = view.findViewById(R.id.enterText1)
        editText2 = view.findViewById(R.id.enterText2)

        val navController = Navigation.findNavController(view)




        button1.setOnClickListener {
            val text = editText.text.toString()
            val text1 = editText1.text.toString()
            val text2 = editText2.text.toString()

            if (text.isEmpty()) {
                return@setOnClickListener
            }


            val action = Fragment1Directions.actionFragment1ToFragment3(text, text1, text2)
            navController.navigate(action)


        }

    }
}